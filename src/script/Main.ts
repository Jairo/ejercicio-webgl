import { mat4, glMatrix } from "gl-matrix";

var VERTEX_RE = /^v\s/;
var FACE_RE = /^f\s/;
var WHITESPACE_RE = /\s+/;

class Main {
    canvas: HTMLCanvasElement;
    gl: WebGLRenderingContext;
    shaderProgram: WebGLProgram;

    vertices:number[] = [];
    faces:number[] = [];

    worldMatrix:mat4;
    projMatrix:mat4;
    time = 0;
  
    constructor() {
        this.canvas = document.createElement('canvas') as HTMLCanvasElement;
        try {
            this.gl = this.canvas.getContext('webgl', { preserveDrawingBuffer: true });
            if (this.gl) {
                this.gl.clearColor(1.0, 1.0, 1.0, 1.0);
                this.gl.clearDepth(1.0);
                this.gl.enable(this.gl.DEPTH_TEST);
                this.gl.depthFunc(this.gl.LEQUAL);
                document.body.appendChild(this.canvas);
                window.onresize = this.resize.bind(this);

                var xhr = new XMLHttpRequest();
                xhr.onload = this.loadOBJ.bind(this);
                xhr.open('GET', './models/teapot.obj');
                xhr.send();

            }
            else {
                throw "Error getting WebGL context - check if your browser supports it";
            }
        } catch (e) {
            console.log(e);
            if (this.shaderProgram != null) this.gl.deleteProgram(this.shaderProgram);
            this.gl = null;
        }
    }
    
    loadOBJ(e:any) {
        var data = e.srcElement.responseText.split('\n');
        
        for (let i = 0; i < data.length; ++i) {
            const line = data[i].trim();
            var lineValues = line.split(WHITESPACE_RE);
            lineValues.shift();

            if (VERTEX_RE.test(line)) {
                this.vertices.push(+lineValues[0] * .01, +lineValues[1] * .01, +lineValues[2] * .01);
            }
            else if (FACE_RE.test(line)) {
                this.faces.push((+lineValues[0]) - 1, (+lineValues[1]) - 1, (+lineValues[2]) - 1);
            }
        }
        
        this.initShaders();
    }

    setVertices() {
        var boxVertexBufferObject = this.gl.createBuffer();
        this.gl.bindBuffer(this.gl.ARRAY_BUFFER, boxVertexBufferObject);
        this.gl.bufferData(this.gl.ARRAY_BUFFER, new Float32Array(this.vertices), this.gl.STATIC_DRAW);
    
        var boxIndexBufferObject = this.gl.createBuffer();
        this.gl.bindBuffer(this.gl.ELEMENT_ARRAY_BUFFER, boxIndexBufferObject);
        this.gl.bufferData(this.gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(this.faces), this.gl.STATIC_DRAW);

        var positionAttribLocation = this.gl.getAttribLocation(this.shaderProgram, 'vertPosition');
        this.gl.vertexAttribPointer(positionAttribLocation, 3, this.gl.FLOAT, false, 3 * Float32Array.BYTES_PER_ELEMENT, 0);
    
        this.gl.enableVertexAttribArray(positionAttribLocation);
    }

    loadTexture() {
        var boxTexture = this.gl.createTexture();
        this.gl.bindTexture(this.gl.TEXTURE_2D, boxTexture);
        this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_WRAP_S, this.gl.CLAMP_TO_EDGE);
        this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_WRAP_T, this.gl.CLAMP_TO_EDGE);
        this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_MIN_FILTER, this.gl.LINEAR);
        this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_MAG_FILTER, this.gl.LINEAR);
    }
  
    drawScene() {
        requestAnimationFrame((time) => {
            this.time = time / 1000;
            this.drawScene();
        });
        
        var identityMatrix = mat4.create();
        this.gl.uniformMatrix4fv(this.gl.getUniformLocation(this.shaderProgram, 'mWorld'), false, this.worldMatrix);
        mat4.rotate(this.worldMatrix, identityMatrix, this.time, [.5, .5, 1]);
        
        this.gl.clear(this.gl.COLOR_BUFFER_BIT | this.gl.DEPTH_BUFFER_BIT);
        this.gl.drawElements(this.gl.TRIANGLES, this.faces.length, this.gl.UNSIGNED_SHORT, 0);
    }

    compileShader(type:number, source:string):WebGLShader {
        let shader = this.gl.createShader(type);
        this.gl.shaderSource(shader, source);
        this.gl.compileShader(shader);
        if (!this.gl.getShaderParameter(shader, this.gl.COMPILE_STATUS)) {
            throw "An error occurred compiling shader: " + this.gl.getShaderInfoLog(shader);
        }

        return shader;
    }

    getShaderFromScript(type:string):string {
        var shader:string;
        for (let i = 0; i < document.scripts.length; i++) {
            const script = document.scripts[i];
            if (script.type == type) shader = script.text;
        }

        return shader;
    }
  
    initShaders() {
        var fragmentShader = this.compileShader(this.gl.FRAGMENT_SHADER, this.getShaderFromScript('x-shader/x-fragment'));
        var vertexShader = this.compileShader(this.gl.VERTEX_SHADER, this.getShaderFromScript('x-shader/x-vertex'));
  
        this.shaderProgram = this.gl.createProgram();
        this.gl.attachShader(this.shaderProgram, vertexShader);
        this.gl.attachShader(this.shaderProgram, fragmentShader);
        this.gl.linkProgram(this.shaderProgram);
    
        if (this.gl.getProgramParameter(this.shaderProgram, this.gl.LINK_STATUS)) {
            this.setVertices();
        
            this.gl.useProgram(this.shaderProgram);
            
            var viewMatrix = mat4.create();
            this.projMatrix= mat4.create();
            this.worldMatrix=mat4.create();
            mat4.identity(this.worldMatrix);
            mat4.lookAt(viewMatrix, [0, 0, -5], [0, 0, 0], [0, 1, 0]);
        
            this.gl.uniformMatrix4fv(this.gl.getUniformLocation(this.shaderProgram, 'mWorld'),false, this.worldMatrix);
            this.gl.uniformMatrix4fv(this.gl.getUniformLocation(this.shaderProgram, 'mView'), false, viewMatrix);
            
            this.resize();
            this.drawScene();
        }
        else {
            throw "Unable to initialize the shader program: " + this.gl.getProgramInfoLog(this.shaderProgram);
        }
    }

    resize() {
        this.canvas.width = window.innerWidth;
        this.canvas.height = window.innerHeight;
        this.gl.viewport(0, 0, this.canvas.clientWidth, this.canvas.clientHeight);
        mat4.perspective(this.projMatrix, glMatrix.toRadian(60), this.canvas.width / this.canvas.height, 1, 10.0);
        this.gl.uniformMatrix4fv(this.gl.getUniformLocation(this.shaderProgram, 'mProj'), false, this.projMatrix);
    }
}

export default Main;
new Main();