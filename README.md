# ejercicio-webgl

ejercicio realizado para ENGWorks

Para compilar y servir el contenido hay que tener [NPM](https://www.npmjs.com) y [Gulp](https://gulpjs.com) instalado

Primero hay que instalar las dependencias
```sh
$ npm i
```

Para compilar (compila dentro de la carpeta *./build*)
```sh
$ gulp build
```

Para ejecutar un servidor que muestre el contenido (en *http://localhost:8000*)
```sh
$ gulp serve
```