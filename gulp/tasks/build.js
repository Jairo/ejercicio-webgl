var gulp = require('gulp');
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var tsify = require('tsify');
var PATH = require('../paths.js');

require('./clean.js');

gulp.task('build-ts', function() {
	return browserify({
        basedir: '.',
        debug: true,
        entries: ['src/script/Main.ts'],
        cache: {},
        packageCache: {}
	})
	.plugin(tsify)
    .bundle()
    .pipe(source('main.js'))
	.pipe(gulp.dest('./' + PATH.BUILD + '/js'));
});

gulp.task('copy-css', function () {
	return gulp.src('src/css/*.css')
		.pipe(gulp.dest('./' + PATH.BUILD + '/css'));
});

gulp.task('copy-images', function () {
	return gulp.src('src/images/*.png')
		.pipe(gulp.dest('./' + PATH.BUILD + '/images'));
});

gulp.task('copy-models', function () {
	return gulp.src('src/models/*.obj')
		.pipe(gulp.dest('./' + PATH.BUILD + '/models'));
});

gulp.task('copy-html', function () {
	return gulp.src('src/html/*.html')
		.pipe(gulp.dest('./' + PATH.BUILD));
});

gulp.task('build',
	gulp.series('clean',
		gulp.parallel(
			'build-ts',
			'copy-css',
			'copy-images',
			'copy-models',
			'copy-html'
)));