var gulp = require('gulp');
var webserver = require('gulp-webserver');
var PATH = require('../paths.js');

gulp.task('serve', function() {
  return gulp.src('./' + PATH.BUILD)
    .pipe(webserver({
      // host: '0.0.0.0',
      livereload: true,
      open: true
    }));
});